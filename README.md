# Rust Lyon Meetup

## Préparation

### Le lieu

Trouver un lieu susceptible d'accueillir le meetup:
- Une salle pouvant accueillir 35/40 personnes
- Des moyens de projection
- Avec un réseau Wifi accessible
- (optionnel) Des moyens de captation image/son
- (optionnel) Bonus s'ils sont OK pour participer à un petit buffet boire/manger

### Les talks

- Trouver deux volontaires pour deux talks entre 10 et 30 minutes
- Deux sujets en rapport avec le langage ou un retour d'expérience
- S'assurer qu'ils ont le matériel pour présenter ou leur en prêter

### La com'

- Annoncer l'évènement sur Meetup, LinkedIn et faire une PR sur ThisWeekInRust
- Préparer les slides d'intro pour le prochain Meetup
- Récupérer les slides des talkers au format PDF et les intégrer au dépôt
  - Donner le choix de la licence (CC-BY-SA 4.0 par défaut)
- Faire des rappels sur LinkedIn & Meetup deux semaines & trois jours en amont

## Le jour J

- Mettre la salle en place
- Briefer les talkers
  - 30 minutes max
  - Si trop de questions, repousser à la 3ème partie du meeting
  - Si pas de micro, répéter la question pour la captation
- Faire le speech d'intro
- Donner la parole aux talkers
  - Chronométrer les talks
  - Faire signe à 10, 5 et 1 minute de la fin
- Inviter à la seconde partie de soirée

### Post-Meetup

- Récupérer les captations et les publier sur la chaine Youtube du Meetup
- Ajouter les liens Youtube et poser un tag dans le dépôt

## Marp

- Outil pour éditer/générer les slides d'intro (HTML, PDF, PPTX)
- S'utilise avec VS Code ou Codium
- Extension à installer : https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode

## Licences

- CC-BY-SA 4.0 ou bien licence particulière si présente dans le dossier du talk

