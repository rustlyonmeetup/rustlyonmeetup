# Meetup #8

- [2024/02/21 - Impl Snake for Micro:bit - Cyril MARPAUD](https://youtu.be/8_Pj6q_mVQw)
- [2024/02/21 - Sécuriser un plugin audio avec Rust - William MUNN](https://youtu.be/US815F9bg5A)

# Meetup #6

- [2023/09/04 - Atrium - Le Serveur Web Polyvalent - Nicolas PERNOUD](https://youtu.be/z43fRaqXLrQ)
- [2023/09/04 - Choisir Rust comme langage ? - Marc-Antoine ARNAUD](https://youtu.be/yuSalA_AFU8)